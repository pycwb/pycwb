from .utils import update_global_var
from .cwb import *
from .cwb_inet2G import *
from .cwb_root_logon import *
from .cwb_xnet import *
