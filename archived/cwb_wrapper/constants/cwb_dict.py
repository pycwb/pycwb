CWB_CAT = {
    "CWB_CAT0": 0,
    "CWB_CAT1": 1,
    "CWB_CAT2": 2,
    "CWB_CAT3": 3,
    "CWB_HVETO": 10,
    "CWB_PEM": 11,
    "CWB_EXC ": 12,
    "CWB_USER": 13
}

CWB_STAGE = {
    "FULL": 0,
    "INIT": 1,
    "STRAIN": 2,
    "CSTRAIN": 3,
    "COHERENCE": 4,
    "SUPERCLUSTER": 5,
    "LIKELIHOOD": 6,
    "SAVE": 7,
    "FINISH": 8,
}